//
//  DetailVC.swift
//  BT-Photos
//
//  Created by Sibani Rani Rath on 28/03/24.
//

import UIKit

class DetailVC: UIViewController {
    
    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAlbumID: UILabel!
    @IBOutlet weak var lblURL: UILabel!
    
    var imageURL = ""
    var photoTitle = ""
    var albumId = 0
    var url = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initDesign()
    }
    
    func initDesign() {
        //Manage for iPhone X
        if UIScreen.main.bounds.height >= 812 {
            navBarHeightConstraint.constant = 92
        }
        
        if let url = URL(string: imageURL) {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                guard let data = data, let image = UIImage(data: data) else { return }
                DispatchQueue.main.async {
                    self.imgView.image = image
                }
            }.resume()
        }
        //self.imgView.image = UIImage(named: image)
        self.lblTitle.text = photoTitle == "" ? "NA" : "Title: \(photoTitle)"
        self.lblAlbumID.text = albumId == 0 ? "NA" : "Album ID: \(String(albumId))"
        self.lblURL.text = url == "" ? "NA" : "URL: \(url)"
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
