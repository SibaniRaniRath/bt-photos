//
//  HomeVC.swift
//  BT-Photos
//
//  Created by Sibani Rani Rath on 27/03/24.
//

import UIKit

struct Photo: Codable {
    let albumId: Int
    let id: Int
    let title: String
    let url: String
    let thumbnailUrl: String
}

class HomeVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var navBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var arrPhotos = [Photo]()
    var currentAlbumID = 1

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initDesign()
    }
    
    func initDesign() {
        //Manage for iPhone X
        if UIScreen.main.bounds.height >= 812 {
            navBarHeightConstraint.constant = 92
        }
        
        // Configure collection view
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        
        // Call function to fetch data
        fetchData()
    }
    
    //MARK: CollectionView Delegate & DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        
        // Remove existing image view if exists
        cell.contentView.subviews.forEach { $0.removeFromSuperview() }

        let imageView = UIImageView(frame: cell.bounds)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .lightGray
        cell.contentView.addSubview(imageView)

        let photo = arrPhotos[indexPath.item]
        if let url = URL(string: photo.url) {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                guard let data = data, let image = UIImage(data: data) else { return }
                DispatchQueue.main.async {
                    imageView.image = image
                }
            }.resume()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let photo = arrPhotos[indexPath.item]
        let detailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:
        "DetailVC") as! DetailVC
        detailVC.imageURL = photo.url
        detailVC.photoTitle = photo.title
        detailVC.albumId = photo.albumId
        detailVC.url = photo.url
        navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3 - 10, height: 150)
    }
    
    //MARK: Api Call
    func fetchData() {
        activityIndicator.startAnimating()
        errorLabel.isHidden = true
        
        let urlString = "https://jsonplaceholder.typicode.com/albums/\(currentAlbumID)/photos"
        guard let url = URL(string: urlString) else {
            showError(message: "Invalid URL")
            return
        }
        
        URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
            }
            if let error = error {
                self.showError(message: "Error fetching data: \(error.localizedDescription)")
                return
            }
            guard let httpResponse = response as? HTTPURLResponse, (200...299).contains(httpResponse.statusCode) else {
                self.showError(message: "Invalid response")
                return
            }
            guard let data = data else {
                self.showError(message: "No data received")
                return
            }
            
            do {
                self.arrPhotos = try JSONDecoder().decode([Photo].self, from: data)
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            } catch {
                self.showError(message: "Error decoding JSON: \(error.localizedDescription)")
            }
        }.resume()
        
    }
    
    func showError(message: String) {
        errorLabel.text = message
        errorLabel.isHidden = false
    }
    
    //MARK: Button Actions
    @IBAction func changeAlbumID(_ sender: UIButton) {
        if let albumIDString = sender.titleLabel?.text, let albumID = Int(albumIDString) {
            currentAlbumID = albumID
            fetchData()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UIColor {
    static func random() -> UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}
